package de.lise.gaming.rating;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.lise.gaming.game.GameEntity;

import javax.persistence.*;

@Entity
public class RatingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ratingId;
    private int evaluation;
    private String comment;
    @ManyToOne(cascade = CascadeType.ALL)
    @JsonIgnore
    private GameEntity game;

    public RatingEntity() {
    }

    public RatingEntity(int evaluation, String comment) {
        this.evaluation = evaluation;
        this.comment = comment;
    }

    public Long getRatingId() {
        return ratingId;
    }

    public void setRatingId(Long ratingId) {
        this.ratingId = ratingId;
    }

    public int getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(int evaluation) {
        this.evaluation = evaluation;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public GameEntity getGame() {
        return game;
    }

    public void setGame(GameEntity game) {
        this.game = game;
    }

}

