package de.lise.gaming.rating;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RatingService {

    private final RatingRepository ratingRepository;

    public RatingService(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    public List<RatingEntity> findAll() {
        return (List<RatingEntity>) ratingRepository.findAll();
    }

    public Optional<RatingEntity> findById(Long ratingId) {
        return ratingRepository.findById(ratingId);
    }

}
