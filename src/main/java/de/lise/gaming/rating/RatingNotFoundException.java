package de.lise.gaming.rating;

public class RatingNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RatingNotFoundException(Long ratingId) {
        super("No rating found with id '" + ratingId + "'");
    }
}
