package de.lise.gaming.rating;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/ratings")
public class RatingController {

    private final RatingService ratingService;

    public RatingController(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @GetMapping
    public List<RatingEntity> list() {
        return ratingService.findAll();
    }

    @GetMapping("{ratingId}")
    public Optional<RatingEntity> getRatingById(@PathVariable Long ratingId) throws RatingNotFoundException {
        return Optional.ofNullable(ratingService.findById(ratingId)
                .orElseThrow(() -> new RatingNotFoundException(ratingId)));
    }
}