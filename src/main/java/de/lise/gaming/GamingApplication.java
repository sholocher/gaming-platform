package de.lise.gaming;

import de.lise.gaming.game.GameEntity;
import de.lise.gaming.rating.RatingEntity;
import de.lise.gaming.game.GameRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class GamingApplication {

    public static void main(String[] args) {
        SpringApplication.run(GamingApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(GameRepository repository) {

        // create ratings
        RatingEntity rating1 = new RatingEntity(5, "Best game ever!");
        RatingEntity rating2 = new RatingEntity(4, "I like");
        RatingEntity rating3 = new RatingEntity(4, "Nice game");
        RatingEntity rating5 = new RatingEntity(2, "Quite okay");
        RatingEntity rating7 = new RatingEntity(3, "I imagined it better 🤔");
        RatingEntity rating8 = new RatingEntity(3, "👍");
        RatingEntity rating9 = new RatingEntity(2, "Part one and two were much better");

        ArrayList<RatingEntity> ratings = new ArrayList<>();
        ratings.add(rating1);
        ratings.add(rating2);
        ratings.add(rating3);
        ratings.add(rating5);
        ratings.add(rating7);
        ratings.add(rating8);
        ratings.add(rating9);

        // create games with ratings and platforms
        GameEntity monkeyIsland = new GameEntity("The Secret of Monkey Island", 1990, "LucasArts", ratings, "https://youtube.com/embed/SZHBGVblNJA?t=21", "The Monkey Island saga is known throughout the gaming world as one of the best point and click adventures, and The Secret of Monkey Island is the game that started it all.", List.of(PlatformType.NINTENDO));
        rating1.setGame(monkeyIsland);
        GameEntity anno1880 = new GameEntity("Anno 1800", 2012, "Blue Byte", ratings, "https://youtube.com/embed/6e1pyccA7Is?t=4", "Lead the Industrial Revolution! Welcome to the dawn of the Industrial Age. The path you choose will define your world. Are you an innovator or an exploiter? A conqueror or a liberator? How the world remembers your name is up to you.", List.of(PlatformType.PC));
        rating8.setGame(anno1880);
        GameEntity darkProject = new GameEntity("Dark Project", 1998, "Looking Glass Studios", ratings, "https://youtube.com/embed/pGRVuWpZ9lE?t=25", "Sneak through the shadows of 12 treacherous missions including haunted cathedrals, subterranean ruins, and forbidding prisons, in a dark and sinister city. Stalk your prey on the quest for stolen goods with your blackjack, sword and an assortment of unique arrows. Steal for money and uncover the hidden agendas of your allies and enemies as you play through an unravelling story of deception and revenge. Survive in a world where shadows are your only ally, trust is not an option, and confrontation results in death!", List.of(PlatformType.PLAYSTATION));
        rating2.setGame(darkProject);
        GameEntity wormsArmageddon = new GameEntity("Worms Armageddon", 1999, "Team17", ratings, "https://youtube.com/embed/Xl2Oox2a58k?t=20", "While this game may look cute, it is in fact as sophisticated and enjoyable as the very best strategy games out there.", List.of(PlatformType.XBOX));
        rating3.setGame(wormsArmageddon);
        GameEntity monkeyIsland2 = new GameEntity("Monkey Island 2: LeChuck's Revenge", 1991, "LucasArts", ratings, "https://youtube.com/embed/tfIG6zcT8uA", "In LeChuck's Revenge Guybrush Threepwood returns in his search for the legendary treasure of Big Whoop. He is now a genuine pirate and has traded in his white shirt and clean shaved face for a dashing blue coat and a beard. This would not be a true sequel if we did not see the return of the villain LeChuck and so, in the true spirit of Monkey Island, Guybrush accidentally helps to bring back LeChuck who, of course, wants revenge", List.of(PlatformType.PC));
        rating7.setGame(monkeyIsland2);
        GameEntity drakan = new GameEntity("Drakan: Order of the Flame", 1999, "Surreal Software", ratings, "https://youtube.com/embed/rKYuc1_SAqc?t=22", " The game follows Rynn, a young woman with extraordinary martial skills, and an ancient dragon Arokh on their quest to free Rynn's younger brother from the evil sorcerer Navaros. The gameplay alternates between dungeon exploration and hack and slash when Rynn is alone and aerial dogfights when she mounts Arokh.", List.of(PlatformType.PLAYSTATION));
        rating5.setGame(drakan);
        GameEntity thief = new GameEntity("Thief: Deadly Shadows", 2004, "Ion Storm", ratings, "https://youtube.com/embed/vPzd8UcGs6E?t=31", "In Thief: Deadly Shadows, gamers once again take on the role of Garrett, a master thief. Garrett is rarely seen, never caught and capable of breaking into the most ingeniously secured places. Garrett steals from the wealthy and gives to himself, making his living in the dark and foreboding City.", List.of(PlatformType.XBOX));
        rating9.setGame(thief);


        return (args) -> {
            // save games
            repository.save(monkeyIsland);
            repository.save(darkProject);
            repository.save(wormsArmageddon);
            repository.save(monkeyIsland2);
            repository.save(drakan);
            repository.save(anno1880);
            repository.save(thief);
        };
    }
}