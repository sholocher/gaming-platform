package de.lise.gaming.game;

import de.lise.gaming.PlatformType;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/games")
public class GameController {

    private final GameService gameService;

    Pageable topThree = PageRequest.of(0, 3);

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    // filter games by platform type
    @GetMapping("/filter")
    public List<GameEntity> getGamesByPlatform(@RequestParam(name = "platform", required = false) List<PlatformType> platforms) {
        if (platforms.isEmpty()) {
            return gameService.findAll();
        } else {
            return gameService.findByPlatformsIn(platforms);
        }
    }

    // search for games containing entered string
    @GetMapping("/search")
    public List<GameEntity> getGamesByTitle(@RequestParam("title") String title) {
        if (title.equals("")) {
            return Collections.emptyList();
        } else {
            return gameService.findGamesByTitleContaining(title);
        }
    }

    // display latest games
    @GetMapping("/latest")
    public List<GameEntity> getLatestGames() {
        return gameService.findByOrderByPublicationDesc(topThree);
    }

    // display most popular games
    @GetMapping("/mostpopular")
    public List<GameEntity> getMostPopularGames() {
        return gameService.findByOrderByRatingsEvaluationDesc(topThree);
    }

    @GetMapping("{gameId}")
    public Optional<GameEntity> getGameById(@PathVariable Long gameId) throws GameNotFoundException {
        return Optional.ofNullable(gameService.findById(gameId)
                .orElseThrow(() -> new GameNotFoundException(gameId)));
    }
}