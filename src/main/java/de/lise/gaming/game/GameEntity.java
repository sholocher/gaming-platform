package de.lise.gaming.game;

import de.lise.gaming.PlatformType;
import de.lise.gaming.rating.RatingEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class GameEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long gameId;
    private String title;
    private int publication;
    private String developer;
    private String youTubeTrailer;
    @Lob
    private String description;
    @ElementCollection
    @Enumerated(EnumType.STRING)
    private List<PlatformType> platforms = new ArrayList<>();
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "game")
    private List<RatingEntity> ratings = new ArrayList<>();

    public GameEntity() {
    }

    public GameEntity(String title, int publication, String developer, List<RatingEntity> ratings, String youTubeTrailer, String description, List<PlatformType> platforms) {
        this.title = title;
        this.publication = publication;
        this.developer = developer;
        this.youTubeTrailer = youTubeTrailer;
        this.description = description;
        this.ratings = ratings;
        this.platforms = platforms;
    }

    public Long getGame_id() {
        return gameId;
    }

    public void setGame_id(Long gameId) {
        this.gameId = gameId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPublication() {
        return publication;
    }

    public void setPublication(int publication) {
        this.publication = publication;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getYouTubeTrailer() {
        return youTubeTrailer;
    }

    public void setYouTubeTrailer(String youtubeTrailer) {
        this.youTubeTrailer = youtubeTrailer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<RatingEntity> getRatings() {
        return ratings;
    }

    public void setRatings(List<RatingEntity> ratings) {
        this.ratings = ratings;
    }

    public List<PlatformType> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<PlatformType> platforms) {
        this.platforms = platforms;
    }

}

