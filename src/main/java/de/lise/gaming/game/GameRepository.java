package de.lise.gaming.game;

import org.springframework.data.repository.CrudRepository;
import de.lise.gaming.PlatformType;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends CrudRepository<GameEntity, Long> {
    List<GameEntity> findAll();
    List<GameEntity> findGamesByTitleContaining(String title);
    List<GameEntity> findByPlatformsIn(List<PlatformType> platforms);
    List<GameEntity> findByOrderByPublicationDesc(Pageable pageable);
    List<GameEntity> findByOrderByRatingsEvaluationDesc(Pageable pageable);
}
