package de.lise.gaming.game;

import de.lise.gaming.PlatformType;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GameService {

    private final GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public List<GameEntity> findAll() {
        return gameRepository.findAll();
    }

    public Optional<GameEntity> findById(Long gameId) {
        return gameRepository.findById(gameId);
    }

    public List<GameEntity> findGamesByTitleContaining(String title) {
        return gameRepository.findGamesByTitleContaining(title);
    }

    public List<GameEntity> findByPlatformsIn(List<PlatformType> platforms) {
        return gameRepository.findByPlatformsIn(platforms);
    }

    public List<GameEntity> findByOrderByPublicationDesc(Pageable pageable) {
        return gameRepository.findByOrderByPublicationDesc(pageable);
    }

    public List<GameEntity> findByOrderByRatingsEvaluationDesc(Pageable pageable) {
        return gameRepository.findByOrderByRatingsEvaluationDesc(pageable);
    }

}
