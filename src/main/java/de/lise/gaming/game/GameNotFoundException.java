package de.lise.gaming.game;

public class GameNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public GameNotFoundException(Long gameId) {
        super("No game found with id '" + gameId + "'");
    }
}
