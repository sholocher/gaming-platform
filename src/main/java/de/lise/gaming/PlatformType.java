package de.lise.gaming;

public enum PlatformType {
    PC,
    PLAYSTATION,
    XBOX,
    NINTENDO
}
