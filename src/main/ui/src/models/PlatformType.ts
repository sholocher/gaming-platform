export enum PlatformType {

    PC = "PC",
    PLAYSTATION = "PLAYSTATION",
    XBOX = "XBOX",
    NINTENDO = "NINTENDO",

}
