import { Rating } from "./Rating";

export interface Game {
  gameId: number;
  title: string;
  publication: number;
  developer: string;
  ratings: Rating[];
  youTubeTrailer: string;
  description: string;
}
