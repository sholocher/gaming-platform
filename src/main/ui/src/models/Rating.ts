import {Game} from "./Game";

export interface Rating {
    ratingId: number;
    evaluation: number;
    comment: string;
    game: Game;
}