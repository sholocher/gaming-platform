import React, { useState } from "react";
import "./GameCard.scss";
import { Rate } from "antd";
import { Game } from "../models/Game";
import { Rating } from "../models/Rating";
import DetailsModal from "../game-details/DetailsModal";
import TrailerFrame from "../shared-components/TrailerFrame";

export default function GameCard({ game }: { game: Game }) {
  const [visible, setVisible] = useState(false);

  const showDetailsModal = () => {
    setVisible(true);
  };

  const closeDetailsModal = (event: any) => {
    if (event.target === event.currentTarget) {
      setVisible(false);
    }
  };

  return (
    <>
      <section className="card" key={game?.title} onClick={showDetailsModal}>
        <TrailerFrame game={game} />
        <h3>{game?.title}</h3>
        {game?.ratings.map((rating: Rating) => (
          <>
            <Rate disabled defaultValue={rating?.evaluation} />
            <p>"{rating?.comment}"</p>
          </>
        ))}
      </section>
      {visible && (
        <DetailsModal game={game} closeDetailsModal={closeDetailsModal} />
      )}
    </>
  );
}
