import React, { useState } from "react";
import { Game } from "../models/Game";
import { PlatformType } from "../models/PlatformType";
import GameCard from "./GameCard";
import "./PlatformFilter.scss";
import { useGetFilteredGames } from "../hooks/useGetFilteredGames";
import Loader from "../Loader";
import filterIcon from "./icon-filter.svg";
import { Checkbox } from "antd";

export default function PlatformFilter() {
  const [platform, setPlatform] = useState();
  // @ts-ignore
  const [{ games, error, loading }] = useGetFilteredGames(platform);
  let filter: any = [];

  const handleClick = (event: any) => {
    if (filter.includes(event.target.value)) {
      let index = filter.indexOf(event.target.value);
      filter.splice(index, 1);
    } else {
      filter.push(event.target.value);
    }
    return filter;
  };

  const handleSubmit = (event: any) => {
    setPlatform(filter);
    event.preventDefault();
  };

  return (
    <section className="all-games">
      <form className="filter-bar" onSubmit={handleSubmit}>
        {Object.keys(PlatformType).map((platformType) => (
          <Checkbox.Group>
            <Checkbox value={platformType} onClick={handleClick}>
              {platformType}
            </Checkbox>
          </Checkbox.Group>
        ))}
        <button className="filter-button" type="submit">
          <img src={filterIcon} alt="Filter icon" /> Filter
        </button>
      </form>
      <div className="cards">
        {loading && <Loader />}
        {error && <p>error</p>}
        {games && games.map((game: Game) => <GameCard game={game} />)}
      </div>
    </section>
  );
}
