import React from "react";
import { useGetSortedGames } from "../hooks/useGetSortedGames";
import "./CardList.scss";
import { Game } from "../models/Game";
import GameCard from "./GameCard";
import { Sorting } from "../models/Sorting";

interface CardListProps {
  sorting: Sorting;
  headline: String;
}

export default function CardList({ sorting, headline }: CardListProps) {
  const [{ games, error, loading }] = useGetSortedGames(sorting);
  return (
    <section className="card-list">
      <h1 className="card-list-name">{headline}</h1>
      <div className="cards">
        {loading && <p>loading...</p>}
        {error && <p>error</p>}
        {games && games.map((game: Game) => <GameCard game={game} />)}
      </div>
    </section>
  );
}
