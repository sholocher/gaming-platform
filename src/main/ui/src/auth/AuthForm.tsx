import React, { useState } from "react";
import "./AuthForm.scss";
import Logo from "../logo.svg";

interface AuthFormProps {
  setLoggedIn: (n: boolean) => void;
}

export default function AuthForm({ setLoggedIn }: AuthFormProps) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const mailRegExp = new RegExp(
    /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
  );
  const passwordRegExp = new RegExp(/.*[0-9].*/);

  function handleSubmit(event: any) {
    event.preventDefault();

    if (!email.match(mailRegExp)) {
      alert("Enter a valid email");
    }
    if (!password.match(passwordRegExp)) {
      alert("Password must contain at least eight characters and one digit.");
    } else {
      setLoggedIn(true);
    }
  }

  return (
    <form className="auth-form" onSubmit={handleSubmit}>
      <img className="login-icon" src={Logo} alt="Games Logo" />
      <input
        className="user-input"
        placeholder="Username"
        type="email"
        onChange={(event) => {
          setEmail(event.target.value);
        }}
      />
      <input
        className="user-input"
        placeholder="Password"
        type="password"
        onChange={(event) => {
          setPassword(event.target.value);
        }}
      />
      <button className="login-button" type="submit">
        Login
      </button>
      <p>Create account</p>
    </form>
  );
}
