import { useState, useEffect } from "react";
import { getGames } from "../api/GameService";
import {Game} from "../models/Game";

export function useGetGames() {
  const [games, setGames] = useState<Game[]>();
  const [error, setError] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);

  async function doGetGames() {
    try {
      setLoading(true);
      setError(false);
      const games = await getGames();
      setGames(games);
    } catch (error) {
      console.error(error);
      setError(true);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    doGetGames();
  }, []);
  return [{ games, error, loading }];
}
