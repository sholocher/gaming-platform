import { useState, useEffect } from "react";
import { getGamesByTitle } from "../api/GameService";
import {Game} from "../models/Game";

export default function useGetGamesByTitle(title: String) {
    const [games, setGames] = useState<Game[]>();
    const [error, setError] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        async function doGetGames() {
            try {
                setLoading(true);
                setError(false);
                const games = await getGamesByTitle(title);
                setGames(games);
            } catch (error) {
                console.error(error);
                setError(true);
            } finally {
                setLoading(false);
            }
        }
        doGetGames();
    }, [title]);
    return [{ games, error, loading }];
}
