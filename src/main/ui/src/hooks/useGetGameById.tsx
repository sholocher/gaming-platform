import {useState, useEffect} from "react";
import {getGameById} from "../api/GameService";
import {Game} from "../models/Game";

export default function useGetGameById(gameId: number) {
    const [games, setGames] = useState<Game[]>();
    const [error, setError] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        async function doGetGame() {
            try {
                setLoading(true);
                setError(false);
                const games = await getGameById(gameId);
                setGames(games);
            } catch (error) {
                console.error(error);
                setError(true);
            } finally {
                setLoading(false);
            }
        }

        doGetGame();
    }, [gameId]);
    return [{games, error, loading}];
}
