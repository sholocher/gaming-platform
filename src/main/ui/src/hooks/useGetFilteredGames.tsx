import { useState, useEffect } from "react";
import { getGamesFilteredByPlatform } from "../api/GameService";
import {Game} from "../models/Game";
import {PlatformType} from "../models/PlatformType";

export function useGetFilteredGames(platform: PlatformType[]) {
    const [games, setGames] = useState<Game[]>();
    const [error, setError] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
    async function doGetGames() {
        try {
            setLoading(true);
            setError(false);
            const games = await getGamesFilteredByPlatform(platform);
            setGames(games);
        } catch (error) {
            console.error(error);
            setError(true);
        } finally {
            setLoading(false);
        }
    }
        doGetGames();
    }, [platform]);
    return [{ games, error, loading }];
}
