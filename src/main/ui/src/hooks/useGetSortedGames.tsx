import { useState, useEffect } from "react";
import {
  getGamesSortedByEvaluation,
  getGamesSortedByDate,
} from "../api/GameService";
import { Game } from "../models/Game";
import { Sorting } from "../models/Sorting";

export function useGetSortedGames(sorting: Sorting) {

  const [games, setGames] = useState<Game[]>();
  const [error, setError] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);

  async function doGetGames() {
    try {
      setLoading(true);
      setError(false);
      let games;
      switch (sorting) {
        case Sorting.BY_EVALUATION:
          games = await getGamesSortedByEvaluation();
          break;
        case Sorting.BY_DATE:
          games = await getGamesSortedByDate();
          break;
      }
      setGames(games);
    } catch (error) {
      console.error(error);
      setError(true);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    doGetGames();
  }, []);
  return [{ games, error, loading }];
}
