import React from "react";

export default function TrailerFrame({ game }: any) {
  return (
    <iframe
      width="100%"
      height="50%"
      frameBorder="0"
      src={game?.youTubeTrailer}
      allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      title={game?.title}
    />
  );
}
