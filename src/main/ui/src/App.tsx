import React, { useState } from "react";
import "./App.scss";
import CardList from "./list-games/CardList";
import { Sorting } from "./models/Sorting";
import PlatformFilter from "./list-games/PlatformFilter";
import AuthForm from "./auth/AuthForm";
import Header from "./header/Header";
import Starter from "./starter.jpeg";

function App() {
  const [loggedIn, setLoggedIn] = useState(false);

  return (
    <>
      <div className="app-container">
        {!loggedIn ? (
          <AuthForm setLoggedIn={setLoggedIn} />
        ) : (
          <>
            <Header setLoggedIn={setLoggedIn} />
            <img src={Starter} className="starter" />
            <section className="content">
              <CardList sorting={Sorting.BY_DATE} headline="Latest" />
              <CardList
                sorting={Sorting.BY_EVALUATION}
                headline="Most Popular"
              />
              <PlatformFilter />
            </section>
          </>
        )}
      </div>
    </>
  );
}

export default App;
