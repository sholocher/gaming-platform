import React, {useState} from "react";
import useGetGamesByTitle from "../hooks/useGetGamesByTitle";
import "./Search.scss";
import Loader from "../Loader";
import {Game} from "../models/Game";
import DetailsModal from "../game-details/DetailsModal";

export default function Search() {
    const [title, setTitle] = useState("");
    const [{games, error, loading}] = useGetGamesByTitle(title);
    const [visible, setVisible] = useState(false);
    let searchedGame: Game | undefined;

    const showDetailsModal = (event: any) => {
        searchedGame = games?.find(
            (searchedGame) =>
                searchedGame.title === event.target.getAttribute("value")
        );
        setVisible(true);
        return searchedGame;
    };

    const closeDetailsModal = (event: any) => {
        if (event.target === event.currentTarget) {
            setVisible(false);
        }
    };

    const handleChange = (event: any) => {
        setTitle(event.target.value);
    };

    return (
        <>
            <div className="search-bar">
                <input
                    className="search"
                    placeholder="Search"
                    type="text"
                    onChange={handleChange}
                />
                <ul className="search-results-container">
                    {loading && <Loader/>}
                    {error && <p>error</p>}
                    {games &&
                    games.map((game: Game) => (
                        <>
                            <li
                                onClick={showDetailsModal}
                                className="search-results"
                                key={game.title}
                                value={game.title}
                            >
                                {game?.title}
                            </li>
                            {visible && searchedGame && (
                                <DetailsModal
                                    game={searchedGame}
                                    closeDetailsModal={closeDetailsModal}
                                />
                            )}
                        </>
                    ))}
                </ul>
            </div>
        </>
    );
}
