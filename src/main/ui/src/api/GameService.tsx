import { Game } from "../models/Game";
import { PlatformType } from "../models/PlatformType";

export async function getGames() {
  return await handleFetch(`/games`);
}

export async function getGameById(gameId: number) {
  return await handleFetch(`/games/${gameId ? gameId : ""}`);
}

export async function getGamesSortedByDate() {
  return await handleFetch(`/games/latest`);
}

export async function getGamesSortedByEvaluation() {
  return await handleFetch("/games/mostpopular");
}

export async function getGamesByTitle(title: String) {
  return await handleFetch(`/games/search?title=${title ? title : ""}`);
}

export async function getGamesFilteredByPlatform(platform: PlatformType[]) {
  return await handleFetch(
    `/games/filter?platform=${platform ? platform : ""}`
  );
}

async function handleFetch(url: String): Promise<Game[]> {
  const response = await fetch(`${url}`, {
    method: "GET",
  });
  if (!response.ok) {
    const errorMessage = await response.text();
    throw new Error(errorMessage);
  }
  return await response.json();
}
