import React, { useState } from "react";
import Search from "../search/Search";
import "./Header.scss";
import Menu from "./menu.svg";

interface HeaderProps {
  setLoggedIn: (x: boolean) => void;
}

export default function Header({ setLoggedIn }: HeaderProps) {
  const [collapsed, setCollapsed] = useState(false);

  const toggleCollapsed = () => {
    setCollapsed(!collapsed);
  };

  return (
    <div className="header">
      <div>
        <h1>Random</h1>
        <img className="brand" />
      </div>
      <Search />
      <img src={Menu} className="side-menu-button" onClick={toggleCollapsed} />
      {collapsed && (
        <ul className="side-menu">
          <button>Account</button>
          <button onClick={() => setLoggedIn(true)}>Logout</button>
        </ul>
      )}
    </div>
  );
}
