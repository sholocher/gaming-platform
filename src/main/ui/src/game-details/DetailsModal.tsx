import { Typography, Rate } from "antd";
import React from "react";
import { Game } from "../models/Game";
import { Rating } from "../models/Rating";
import "./DetailsModal.scss";
import TrailerFrame from "../shared-components/TrailerFrame";

interface DetailsModalProps {
  game: Game;
  closeDetailsModal: (event: React.MouseEvent) => void;
}

export default function DetailsModal({
  game,
  closeDetailsModal,
}: DetailsModalProps) {
  const { Paragraph } = Typography;
  return (
    <div className="modal" onClick={closeDetailsModal}>
      <div className="game-details">
        <TrailerFrame game={game} />
        <h1 className="game-title">{game?.title}</h1>
        <h2>Developer: {game?.developer}</h2>
        <h2>Released: {game?.publication}</h2>
        <Paragraph ellipsis={{rows: 3 }} className="game-description">{game?.description}</Paragraph>
        {game?.ratings.map((rating: Rating) => (
          <section>
            <Rate disabled defaultValue={rating?.evaluation} />
            <h3>"{rating?.comment}"</h3>
          </section>
        ))}
      </div>
    </div>
  );
}
